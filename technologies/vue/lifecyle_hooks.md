# Lifecycle hooks: le cycle de vie d'un composant

[Documentation officielle](https://vuejs.org/guide/essentials/lifecycle.html)

[Liste complète des hooks](https://vuejs.org/api/composition-api-lifecycle)

---

## Le concept

Javascript et Vue sont asynchrones, donc *tout n'est pas disponible à tout moment*:
un élément peut ne pas être chargé, il peut avoir été détruit... Il y a toujours un
temps entre le moment où un composant un appelé et celui où ce composant est prêt à
être utilisé (c'est-à-dire, qu'il a été inséré dans le DOM).

Les `lifecycle hooks`, c'est ce qui permet de gérer les cycles de vie des composants.
Concrètement, un `hook` est une fonction qui est lancée lorsqu'une certaine étape
du cycle de vie d'un composant est atteinte: quand il est créé, détruit, mis à jour,
quand il y a une erreur...

Une analogie: si on a un objet `plante` et qu'on veut récolter ses fleurs avec la
fonction `coupeFleurs`. Il faut attendre la floraison pour avoir des fleurs à couper. 
La floraison est donc une étape du cycle de vie de notre composant `plante`.
Quand la floraison arrive, une action est activée: `coupeFleurs()`. Sans `lifecycle hooks`,
on se lancerait `coupeFleurs` dès le début, sans fleurs à couper: il y aurait une
erreur.

Les plus importants sont:
- `onMounted`: l'élément a été créé, c'est-à-dire qu'il a été inséré dans le DOM
- `onUnmounted`: l'élément a été retiré du DOM.

---

## La technique

Les `hooks` sont représentés par des fonctions définies par Vue. Ces fonctions
fonctionnent de manière analogue à des `addEventListener`: lorsque une étape du 
cycle de vie est atteinte, alors la fonction définie par le `hook` est lancée 
(comme quand on clique sur un élément HTML et que ça lance une fonction).

Pour pouvoir écouter une étape du cycle de vie, il faut importer le `hook` associé,
et la fonction sera lancée au bon moment. C'est tout.

```javascript
import { onMounted } from 'vue'

onMounted(() => {
  console.log(`the component is now mounted.`)
})
```

---

## Un exemple

Soit un élément:

```html
<template>
  <div>
    <ul>
      <li>Linkin Park</li>
      <li>Korn</li>
      <li>System of a Down</li>
    </ul>
  </div>
</template>
```

Si on veut accéder à `li:first-child`, il faut que tout cet élément ait été 
inséré dans le DOM. Vue gère ça de façon asynchone, c'est-à-dire que l'insertion
de l'élément n'est pas instantanée (d'autant plus quand on affiche le résultat
d'une requête AJAX). Il faut donc attendre que l'élément ait été `mounted`, avec
la fonction `onMounted()`:

```javascript
import { onMounted } from "vue";

onMounted(() => {
  console.log( document.querySelector("li:first-child") )
});
```

---

## Le schéma du cycle de vie

(Ce schéma de la doc officielle est très complexe, notre appli est relativement
petite, donc on aura rarement besoin de tous les `hooks` définis. Ne pas se laisser
perturber).

![Cycle de vie d'un composant sur Vue](./img/vue_lifecycle.png)
