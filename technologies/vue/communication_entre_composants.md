# Communication entre les composants: `emit`, `props`, `stores`

[Documentation officielle](https://vuejs.org/guide/essentials/component-basics.html)

Le père, le fils et le saint-esprit.

Par défaut, deux composants parent / enfant sont isolés l'un de l'autre:
les variables et données définies le parent ne sont pas accessibles à 
l'enfant, et inversement. Dans une application, il est souvent important
de faire communiquer des éléments: passer des données
- du parent à l'enfant,
- de l'enfant au parent
- entre plusieurs éléments qui n'ont pas nécessairement des liens hiérarchiques.

De façon basique, la communication parent-enfant et enfant-parent se fait
en indiquant dans le `<script>` que des données sont faites pour être communiquées,
et en passant ces données au bon élément dans le `<template>`.

En reprenant les exemples de la doc officielle, on a deux composants:
- `App.vue`, le parent qui contient une liste de posts de blog
- `Blogpost.vue`, l'enfant, qui est un unique post de blog.

```html
<!-- App.vue -->
<template>
  <h1>Mon joli site</h1>
  <Blogpost />
</template>
```

Dans les exemples ci-dessous, `App.vue` envoie un `title` à `Blogpost.vue`,
et `Blogpost.vue` contient un bouton *enlarge text* qui permet d'augmenter
la taille du texte dans le parent.

---

## Parent -> enfant: `props`

On communique un titre de `App.vue` vers `Blogpost.vue`. L'idée, c'est qu'on passe 
une donnée du parent à l'enfant via un attribut HTML, au niveau du parent. Dans 
l'enfant, on déclare cet attribut passé par le parent comme un `props`, c'est-à-dire
un élément pouvant être passé du parent à l'enfant.

```html
<!-- App.vue -->
<Blogpost :title="uneVariable">  // `uneVariable` est une variable telle qu'elle est définie dans le script
```

Dans le javascript de l'enfant, on indique tous les noms d'attribut qui sont des `props` 
envoyés par le parent. On utilise la fonction `defineProps`. Elle prend une liste (les noms d'attributs
envoyés du parent à l'enfant) et toutes ces valeurs sont accessibles depuis l'enfant:

```javascript
const props = defineProps(['title']);  // le contenu de l'attribut 'title' peut maintenant être transmise à l'enfant
console.log(props.title);
```

La variable `props` définie ci-dessus contient un dictionnaire dans lequel un 
prop (défini par un nom d'attribut, dans le parent) est associé à une valeur
(valeur de l'attribut définie dans le parent).

En bref, dans le parent, on a:

```html
<!-- app.vue -->
<script setup>
import BlogPost from './BlogPost.vue'

const uneVariable = "omg hiii";
</script>

<template>
	<BlogPost
  	:title="uneVariable"
	></BlogPost>
</template>
```

Et dans l'enfant, on a:

```html
<script setup>
defineProps(['title', 'id'])
</script>

<template>
  <h4>{{ title }}</h4> <!-- affichera `omg hiii`, défini par `uneVariable` dans le parent -->
</template>

