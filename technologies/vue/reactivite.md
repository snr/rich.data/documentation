# Réactivité: suivre les changements d'état, modifier et accéder dynamiquement à des valeurs

---

## `Ref`

---

## `Reactive`

---

## `Computed properties`

[La doc](https://vuejs.org/guide/essentials/computed.html)

De façon basique, une propriété `computed` est une propriété d'un composant qui est 
**calculée au moment de l'exécution** et qui peut inclure des **données réactives**.

```html
<script setup>
import { reactive, computed } from 'vue'

// un objet réactif
const author = reactive({
  name: 'John Doe',
  books: [
    'Vue 2 - Advanced Guide',
    'Vue 3 - Basic Guide',
    'Vue 4 - The Mystery'
  ]
})

// une propriété computed: si `authors.books`
// est un `array` vide, alors le résultat sera `No`.
const publishedBooksMessage = computed(() => {
  return author.books.length > 0 ? 'Yes' : 'No'
})
</script>
```

Une `computed property` **retourne une `computed ref`**, objet similaire
à `ref`: pour accéder à la valeur définie par `publishedBooksMessage`,
il faudra utiliser `publishedBooksMessage.value`.

Une `computed property` **prend en compte les changements de valeurs dans les propriétés
réactives auxquelles elle accède**. Dans l'exemple au dessus, si `author.books` change,
alors le résultat renvoyé par `publishedBooksMessage` changera en conséquence.

Une `computed property` **permet d'écouter les changements dans les `props` et `emits`**,
(voir [ce post](<https://stackoverflow.com/a/70631776/17915803))
c'est-à-dire dans les données reçues d'autres composants. C'est très utile quand on
arrive pas à écouter les changements d'une autre manière: ce n'est pas toujours évident 
d'utiliser `watch()` sur un `props`:

```javascript
// apiTarget est une URL définie dans le parent et reçue comme `props`
const props = defineProps([ "apiTarget" ]);

// ne fera rien quand la valeur change
watch(props.apiTarget, (newApiTarget, oldApiTarget) => {
  console.log("apiTarget changed!");
})

// réagira au changement de valeur
watch(computedApiTarget, (newApiTarget, oldApiTarget) => {
  console.log("computedApiTarget changed!");
}))
```

---

## State: la gestion d'état 

---

## `Watchers`

[La doc](https://vuejs.org/guide/essentials/watchers.html)
