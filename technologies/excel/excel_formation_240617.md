# FORMATION EXCEL

Formation donnée par Federico Nurra.

## Introduction

Excel est une feuille de calcul, pas une base de données. Le vocabulaire:
- `record`: une ligne. Les `records` sont numérotées de 1 à n.
- `column`: colonne. Les colonnes sont numérotées alphabétiquement. Le titre de la colonne
  fonctionne comme une clé d'un JSON qui donne accès à une valeur.
- chaque cellule est située dans la feuille par sa position ligne/colonne: `A1`, `C24`.
- toujours avoir un éditeur de texte brut ouvert en même temps que Excel: Excel cache des
  choses, modifie des données sans le montrer, donc il faut passer par le fichier en texte
  brut, pour retraiter.

## Au niveau du tableur

La première chose à faire, c'est de définir les **formats de cellule**. 
- avec `Format de cellule`, on sélectionne tout le tableur et on définit 
  le format comme étant du `texte`
- l'intérêt, c'est que excel ne réinterprète pas nos données (reformate les dates...),
  ce qui peut les corrompre.

**Les différentes feuilles**:
- on doit définir une feuille par type de ressource / par objet documentaire (plutôt qu'une 
  feuille par institution): une feuille icono, une feuille localisations... 
- L'intérêt, c'est d'éviter de se retrouver avec 15 structures de données différentes pour 
  un seul objet, et de réduire les corrections à faire en centralisant tout.


## Au niveau de la feuille

La première colonne doit contenir des **identifiants uniques**, qui ne comprend pas d'espace.
- il peut être aléatoire ou non.

Les **couleurs** n'ont aucun sens pour excel, mais peuvent être utilisées par nous pour mieux
comprendre nos données.

Champs contrôlés: on peut définir des **listes** ou plages de données. Quand on a une série
de données avec seulement quelques valeurs qui se répètent, comme les institutions, on peut
contrôler les valeurs:
- Créer une feuille `institution`.
- Dans cette feuille, créer une colonne avec tous les noms d'institutions
- On créer une liste:
  - Dans LibreOffice, `données > définir la plage > <sélectionner la colonne>`. 
  - Dans Excel, quelque chose de similaire.
  - On nomme la liste, c'est comme ça qu'on pourrra la réutiliser ailleurs.
- **Attention**: les listes doivent être recréées quand on modifie les valeurs.  
- Ensuite, quand on veut utiliser cette liste dans une autre feuille:
  - On sélectionne la colonne
  - Dans Excel, `données > validation des données`:
    - `critères de validation > autoriser`: Sélectionner `Liste`
    - `source`: comme on a nommé sa liste de valeurs, on donne le nom de 
      cette liste avec un `=` avant: `=institutions` pour cibler une liste
      nommée `institutions`.

## Au niveau de la colonne

Privilégier les champs multivalués à plusieurs colonnes type `Auteur 1, Auteur 2`...

**Splitter une colonne** sur un délimiteur:
- `Données > convertir`, on choisir son déilimiteur et pif paf pouf.

**Produire un index de valeurs dédupliquées** à partir d'une colonne à champs multivalués
avec doublons
- Par exemple, à partir de notre colonne `theme`, créer une liste dédupliquée de thèmes.
- **Copier la colonne** dans un éditeur de texte
  - Faire des retour à la ligne à partir des champs multivalués: `\s*\|\s*` -> `\n` 
    (ou `\r\n` sur Windows)
 - Supprimer les espaces en début et fin de ligne: `(^\s+|\s*$)` -> ''. (on remplace par du 
   vide pour supprimer)
 - Réimporter la colonne dans Excel, 
 - Supprimer les doublons: `données > ...` 
 - Trier alphabétiquement: `données > filtres > trier alphabétiquement`

**Vérifier les données dans une colonne multivaluée**:
- On crée une feuille avec notre index de thèmes, et on en crée une liste de données pour valider.
  - La liste est nommée `theme_validateur`
- On split la colonne à valider `theme` par `pipe` pour avoir plusieurs colonnes de scalaires.
- Sur les colonnes, on ajoute une règle de validation avec `=theme_validateur`.
- Reconcaténer les colonnes:
  - créer une nouvelle colonne `theme_concat` dans notre feuille icono
  - la remplir avec une fonction. On suppose que les colonnes de thèmes sont dans les colonnes
    `C,D,E` et qu'on est sur la première ligne, `1`. 
  - la fonction, c'est `=CONCAT(C1;"|";D1;"|";E1)`, soit
    - `=CONCAT()`: la fonction
    - `C1, D1, E1`: les cellules à concaténer
    - `;`: le séparateur entre les différentes colonnes à concaténer
    - `"|`: notre séparateur final.
    - on peut fancifier la chose la chose avec des `IFNULL` etc.
  - enfin, étirer la fonction sur tous les records nécessaires.


**Recherche verticale**: l'équivalent de la jointure, ça consiste à importer dans une table
les données d'une autre.
- `RECHERCHEV` ou `VLOOKUP`: la fonction à utiliser


