SELECT 
  place.id_uuid
  , place.date
  , CONCAT(address.number, ' ', address.street)
  , cartography.granularity
  , filename.url
FROM place
JOIN r_address_place
  ON r_address_place.id_place = place.id
JOIN address
  ON r_address_place.id_address = address.id
  AND address.source = place.vector_source
JOIN r_cartography_place 
  ON r_cartography_place.id_place = place.id
JOIN cartography 
  ON cartography.id = r_cartography_place.id_cartography
  AND cartography.map_source = place.vector_source
JOIN filename
  ON filename.id_cartography = cartography.id
;
