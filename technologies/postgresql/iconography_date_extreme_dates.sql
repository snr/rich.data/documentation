(
  SELECT lower(iconography.date)
  FROM iconography
  WHERE iconography.date IS NOT NULL
  ORDER BY iconography.date ASC
  LIMIT 1
)
UNION 
(
  SELECT upper(iconography.date)
  FROM iconography
  WHERE iconography.date IS NOT NULL
  ORDER BY iconography.date DESC
  LIMIT 1
);
