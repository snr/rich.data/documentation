--
-- check the relationship between place, cartography and iconography
--

SELECT title.entry_name AS iconography
       , iconography.date AS iconography_date
       , cartography.title AS cartography
       , "place".centroid
FROM place

JOIN r_iconography_place 
  ON r_iconography_place.id_place = place.id
JOIN iconography 
  ON iconography.id = r_iconography_place.id_iconography
JOIN r_cartography_place
  ON r_cartography_place.id_place = place.id
JOIN cartography
  ON r_cartography_place.id_cartography = cartography.id
JOIN title
  ON title.id_iconography = iconography.id
  
WHERE cartography.map_source = 'feuille' AND title.ismain IS TRUE
;