--
-- view the results of the database insertion on the Iconography ressources
--

SELECT iconography.date
       , title.entry_name AS title
       , actor.entry_name AS author
       , theme.entry_name AS theme
       , named_entity.entry_name AS named_entity
       , iconography.iiif_url
FROM iconography
LEFT JOIN title 
  ON title.id_iconography = iconography.id 
  AND title.ismain IS TRUE
LEFT JOIN r_iconography_named_entity
  ON r_iconography_named_entity.id_iconography = iconography.id
LEFT JOIN named_entity
  ON r_iconography_named_entity.id_named_entity = named_entity.id
LEFT JOIN r_iconography_actor 
  ON iconography.id = r_iconography_actor.id_iconography
  AND iconography.date IS NOT NULL
LEFT JOIN actor 
  ON actor.id = r_iconography_actor.id_actor 
  AND r_iconography_actor.ismain
  AND r_iconography_actor.role = 'author'
LEFT JOIN r_iconography_theme 
  ON r_iconography_theme.id_iconography = iconography.id
LEFT JOIN theme 
  ON r_iconography_theme.id_theme = theme.id
;
