-- [1821,1830) correspond à la tranche de dates 1821-1829
-- (du début de 1821 inclus au début de 1830 non-inclus)

-- dateBefore exclusif
-- SELECT upper( '[1821,1830)'::int4range ) <= 1825;

-- dateBefore inclusif
-- SELECT lower( '[1821,1830)'::int4range ) <= 1825;

-- dateAfter exclusif
--- SELECT lower( '[1821,1830)'::int4range ) >= 1825;

-- dateAfter inclusif
-- SELECT upper( '[1821,1830)'::int4range ) >= 1825;

-- dateExact: c'est forcément exclusif
-- SELECT '[1821,1830)'::int4range = int4range(1821,1821+1);

-- dateRange exclusif: les 2 range sont égaux
-- SELECT '[1821,1830)'::int4range = int4range(1821,1830);

-- dateRange inclusif: calculer l'intersection
SELECT NOT isempty( '[1821,1830)'::int4range * int4range(1823,1826) );
